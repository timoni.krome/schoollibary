package schoollibary;

public class Stack<contentType> {

	/* Beginn innere Klasse */
	private class node {
		node lowerNode = null;
		contentType content;
		
		
		
		public node(node lowerNode, contentType content) {
			
			if(content != null) {
				this.lowerNode = lowerNode;
				this.content = content;
			}
			
			
		}
		
		public node getLowerNode() {
			return lowerNode;
		}
		
		public contentType getContent() {
			return content;
		}
	}
	/* Ende innere Klasse */
	
	
	//Variablendeklaration
	private node top;
	
	
	//Objekt zum Stapel hinzufügen
	public void push(contentType content) {
		if(content != null) {
			top = new node(top, content);
		}
	}
	
	
	//Objekt vom Stapel entfernen
	public void pop() {
		if(top != null) {
			top = top.lowerNode;
		}
	}
	
	
	// leeregehalt überprüfen
	public boolean isEmpty() {
		return null == top;
	}
	
	
	//Oberstes Objekt vom Stapel zurückgeben
	public contentType Top() {
		return top.getContent();
	}
}
