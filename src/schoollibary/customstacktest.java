/**
 * 
 */
package schoollibary;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author timo
 *
 */
class customstacktest {

	@Test
	void test() {
		Stack<Integer> test = new Stack<>();
		test.push(Integer.valueOf(2));
		test.push(Integer.valueOf(6));
		
		if(test.Top().intValue() != 6) {
			fail("value is: " + test.Top().intValue()+  " instead of 6");
		}
		
		test.pop();
		
		if(test.Top().intValue() != 2) {
			fail("wrong value. it should be 2 instead of: " + test.Top().intValue());
		}
		
		
		
	}

}
